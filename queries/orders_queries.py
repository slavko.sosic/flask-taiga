import os
import sys

sys.path.append(os.getcwd())
from database.db import cursor, db
from mysql.connector import Error
from queries.product_queries import get_product


def add_order(order):
    product_from_db = get_product(order.product_code)[0]
    if product_from_db is None:
        return 'Product not found', 404

    customer_from_db = get_customer(order.customer_number)
    if customer_from_db[0] is None:
        return 'Customer not found', 404
    # provjera da li imamo dovoljno u skladistu
    sql = (f"SELECT quantityInStock - {order.quantity_ordered} AS remaining_stock FROM "
           "(SELECT quantityInStock "
           "FROM classicmodels.products "
           f"WHERE productCode = '{order.product_code}'"
           ") AS in_stock;")
    cursor.execute(sql)
    result = cursor.fetchone()[0]
    if result < 0:
        return 'Too many products ordered', 404

    sql = (
        "INSERT INTO `classicmodels`.`orders` "
        "(`orderNumber`, `orderDate`, `requiredDate`, `status`, `comments`,`shippedDate`, `customerNumber`) "
        "VALUES (%(order_number)s, %(order_date)s, %(required_date)s, %(status)s, %(comments)s, %(shipped_date)s, "
        "%(customer_number)s)"
    )
    try:
        cursor.execute(sql, order)
        db.commit()
        sql = (
            "INSERT INTO `classicmodels`.`orderdetails` "
            "(`orderNumber`, `productCode`, `quantityOrdered`, `priceEach`, `orderLineNumber`) "
            f"VALUES (%(order_number)s, %(product_code)s, %(quantity_ordered)s, {product_from_db[7]}, %(order_line_number)s)"
        )
        cursor.execute(sql, order)
        db.commit()

    except Error as error:
        return error.msg, 404
    print(sql)
    # Use one try and except
    return 'Order shipped!', 201


def get_customer(customer_number):
    sql = (f"select * from `classicmodels`.`customers` "
           f"where customerNumber={customer_number}")
    try:
        cursor.execute(sql)
        result = cursor.fetchone()
        return result, 200
    except Error as error:
        return error.msg, 404
