import os
import sys

sys.path.append(os.getcwd())
from database.db import cursor, db
from mysql.connector import Error


def get_all():
    sql = ('SELECT * FROM classicmodels.employees')
    cursor.execute(sql)
    result = cursor.fetchall()
    dict_first = {f'employee_{employee[0]}': employee for employee in result}
    return dict_first


def add_employee(employee):
    sql = (
        'INSERT INTO `classicmodels`.`employees` ( `employeeNumber`, `lastName`, `firstName`, '
        '`extension`, `email`, `officeCode`, `reportsTo`, `jobTitle`)'
        f"VALUES ('{employee.employeeNumber}', '{employee.lastName}',"
        f"'{employee.firstName}','{employee.extension}',"
        f"'{employee.email}','{employee.officeCode}',"
        f"'{employee.reportsTo}', '{employee.jobTitle}');")
    try:
        cursor.execute(sql)
        db.commit()
        return True, 201
    except Error as error:
        return error.msg, 404
    # Return message and status code for each case
    # corrected


def get_one(employ_number):
    sql = (f'select * from `classicmodels`.`employees` '
           f'where employeeNumber={employ_number}')
    try:
        cursor.execute(sql)
        result = cursor.fetchone()
        return result
    except Error as error:
        return error.msg


def update_employee(employee, employee_number):
    employee_from_db = get_one(employee_number)
    if not employee_number.isnumeric():
        return 'Invalid number', 404

    if employee_from_db is None:
        return 'Employee not found', 404

    changed_employee = {'employeeNumber': employee_from_db[0], "lastName": employee_from_db[1],
                        'firstName': employee_from_db[2], 'extension': employee_from_db[3],
                        'email': employee_from_db[4], 'officeCode': employee_from_db[5],
                        'jobTitle': employee_from_db[7], 'reportsTo': employee_from_db[6]}
    changed_employee.update({key: value for key, value in employee.items() if value})
    sql = (f"UPDATE `classicmodels`.`employees` SET  lastName='{changed_employee['lastName']}', "
           f"firstName='{changed_employee['firstName']}', extension='{changed_employee['extension']}',"
           f" email='{changed_employee['email']}', officeCode='{changed_employee['officeCode']}',"
           f" jobTitle='{changed_employee['jobTitle']}', reportsTo='{changed_employee['reportsTo']}'"
           f" where employeeNumber={employee_number}")
    try:
        cursor.execute(sql)
        db.commit()
        return changed_employee, 201
    except Error as error:
        return error.msg, 404


def delete_employee(employee_number):
    if not employee_number.isnumeric():
        return 'Invalid number', 404
    employee_from_db = get_one(employee_number)
    if employee_from_db is None:
        return 'Employee not found', 404

    sql = (f'DELETE FROM `classicmodels`.`employees`'
           f' WHERE employeeNumber={employee_number}')
    try:
        cursor.execute(sql)
        db.commit()
        return 'Employee deleted', 200
    except Error as error:
        return error.msg, 404
