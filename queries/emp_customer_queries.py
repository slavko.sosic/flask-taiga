import os
import sys

sys.path.append(os.getcwd())
from database.db import cursor, db
from mysql.connector import Error


def get_employee_customers(salesRepEmployeeNumber):
    if not salesRepEmployeeNumber.isnumeric():
        return 'Invalid number', 404

    sql = (f'select * from `classicmodels`.`customers` '
           f'where salesRepEmployeeNumber={salesRepEmployeeNumber}')
    try:
        cursor.execute(sql)
        result = cursor.fetchall()
        return result, 200
    except Error as error:
        return error.msg, 404
