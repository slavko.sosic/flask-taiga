import os
import sys

sys.path.append(os.getcwd())
from database.db import cursor, db
from mysql.connector import Error
from datetime import datetime


def get_payments(info_data):
    sql = (f"select * from classicmodels.payments where customerNumber = {info_data.customerNumber} "
           f"and paymentDate > '{info_data.paymentDate}' order by paymentDate asc;")
    try:
        cursor.execute(sql)
        result = cursor.fetchall()
        return result, 200
    except Error as error:
        return error.msg, 404


def do_payment(order_number):
    order_from_db = get_order(order_number)[0]
    if order_from_db is None:
        return 'Order not found', 404
    sql = (f'select quantityOrdered * priceEach from'
           f' classicmodels.orderdetails where orderNumber = {order_number};')
    try:
        cursor.execute(sql)
        amount = cursor.fetchone()[0]
        current_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        sql = (f"INSERT INTO `classicmodels`.`payments` (`customerNumber`, `checkNumber`, `paymentDate`, `amount`) "
               f"VALUES ('{order_from_db[6]}', 'AE215433', '{current_date}', '{amount}');")
        cursor.execute(sql)
        db.commit()
        return 'Payment is saved', 201
    except Error as error:
        return error.msg, 404
    # use one try and except
    # corrected


def get_order(order_numbe):
    sql = (f"select * from `classicmodels`.`orders` "
           f"where orderNumber='{order_numbe}'")
    try:
        cursor.execute(sql)
        result = cursor.fetchone()
        return result, 200
    except Error as error:
        return error.msg, 404
