import os
import sys

sys.path.append(os.getcwd())
from database.db import cursor, db
from mysql.connector import Error


def add_product(product):
    sql = (
        'INSERT INTO `classicmodels`.`products` '
        '( `productCode`, `productName`, `productLine`,'
        ' `productScale`, `productVendor`, `productDescription`,'
        '`quantityInStock`, `buyPrice`, `MSRP`)'
        f"VALUES ('{product.productCode}', '{product.productName}',"
        f"'{product.productLine}','{product.productScale}',"
        f"'{product.productVendor}','{product.productDescription}',"
        f"'{product.quantityInStock}', '{product.buyPrice}', '{product.MSRP}');")
    try:
        cursor.execute(sql)
        db.commit()
        return True
    except Error as error:
        return error.msg


def get_product(product_code):
    sql = (f"select * from `classicmodels`.`products` where productCode='{product_code}'")
    try:
        cursor.execute(sql)
        result = cursor.fetchone()
        return result, 200
    except Error as error:
        return error.msg, 404

def get_all():
    sql = ('SELECT * FROM classicmodels.products')
    cursor.execute(sql)
    result = cursor.fetchall()
    return result

def delete_product(product_code):
    product_from_db = get_product(product_code)
    if product_from_db[0] is None:
        return 'Product not found', 404

    sql = (f"DELETE FROM `classicmodels`.`products` WHERE productCode='{product_code}'")
    try:
        cursor.execute(sql)
        db.commit()
        return 'Product deleted', 200
    except Error as error:
        return error.msg, 404


def update_product(product, product_code):
    product_from_db = get_product(product_code)[0]

    if product_from_db is None:
        return 'Product not found', 404
    changed_product = {
        "productCode": product_from_db[0],
        "productName": product_from_db[1],
        "productLine": product_from_db[2],
        "productScale": product_from_db[3],
        "productVendor": product_from_db[4],
        "productDescription": product_from_db[5],
        "quantityInStock": product_from_db[6],
        "buyPrice": product_from_db[7],
        "MSRP": product_from_db[8]
    }
    changed_product.update({key: value for key, value in product.items() if value})

    sql = (
        "UPDATE `classicmodels`.`products` "
        "SET "
        "`productName` = %(productName)s, "
        "`productLine` = %(productLine)s, "
        "`productScale` = %(productScale)s, "
        "`productVendor` = %(productVendor)s, "
        "`productDescription` = %(productDescription)s, "
        "`quantityInStock` = %(quantityInStock)s, "
        "`buyPrice` = %(buyPrice)s, "
        "`MSRP` = %(MSRP)s "
        "WHERE `productCode` = %(productCode)s"
    )
    try:
        cursor.execute(sql, changed_product)
        db.commit()
        return changed_product, 201
    except Error as error:
        return error.msg, 404

