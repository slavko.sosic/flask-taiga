import os
import sys

sys.path.append(os.getcwd())
from database.db import cursor, db
from mysql.connector import Error


def insert_customer(customer):
    sql = (
        'INSERT INTO `classicmodels`.`customers` ( `customerNumber`, `customerName`, `contactLastName`, '
        '`contactFirstName`, `phone`, `addressLine1`, `addressLine2`, `city`, `state`, `postalCode`, `country`, '
        '`salesRepEmployeeNumber`, `creditLimit`)'
        f"VALUES ('{customer.customer_number}', '{customer.customer_name}', '{customer.contact_last_name}', "
        f"'{customer.contact_first_name}', '{customer.phone}', '{customer.address_line1}', '{customer.address_line2}', "
        f"'{customer.city}', '{customer.state}', '{customer.postal_code}', '{customer.country}', '{customer.sales_rep_employee_number}', '{customer.credit_limit}');")
    try:
        cursor.execute(sql)
        db.commit()
        return True
    except Error as error:
        return error

def get_all():
    sql = ('SELECT * FROM classicmodels.customers;')
    cursor.execute(sql)
    result = cursor.fetchall()
    dict_first = {f'customer_{custom[0]}': custom for custom in result}
    return dict_first

