import os
import sys
from decimal import Decimal

sys.path.append(os.getcwd())
from flask import Flask, jsonify
from flask_restful import Api, Resource, reqparse, abort
from queries.payments_queries import get_payments, do_payment

customer_post_args = reqparse.RequestParser()
customer_post_args.add_argument("customerNumber", type=int, help="Should be customer number of the person :)",
                                required=True)
customer_post_args.add_argument("paymentDate", type=str, help="Should be date in yyyy-mm-dd format :)",
                                required=True)


def parm_is_here(param):
    return True if param else False


class Customer_payment(Resource):
    def post(self):
        args = customer_post_args.parse_args()
        payments = get_payments(args)
        return jsonify(payments[0]) if payments[1] == 200 else (payments[0], payments[1])

    def get(self, order_number=None):
        if parm_is_here(order_number):
            res = do_payment(order_number)
            return res
        abort(404, message="Missing param")
