import os
import sys

sys.path.append(os.getcwd())
from flask import Flask, jsonify, request
from flask_restful import Api, Resource, reqparse, abort
from queries.emp_customer_queries import get_employee_customers


class Employees_customers(Resource):
    def get(self):
        param = request.args.get('employee_id')
        res = get_employee_customers(param)
        return jsonify(res[0]) if res[1] == 200 else (res[0], res[1])


