import os
import sys

sys.path.append(os.getcwd())
from flask import Flask, jsonify
from flask_restful import Api, Resource, reqparse, abort
from queries.orders_queries import add_order

order_post_args = reqparse.RequestParser()
order_post_args.add_argument('product_code', type=str, required=True, help='Product code is required')
order_post_args.add_argument('quantity_ordered', type=int, required=True, help='Quantity is required')
order_post_args.add_argument('customer_number', type=int, required=True, help='Customer number is required')
order_post_args.add_argument('order_number', type=int, required=True, help='Order number is required')
order_post_args.add_argument('order_date', type=str, required=True, help='Order date is required')
order_post_args.add_argument('required_date', type=str, required=True, help='Required date is required')
order_post_args.add_argument('status', type=str, required=True, help='Status is required')
order_post_args.add_argument('comments', type=str, required=True, help='Comments are required')
order_post_args.add_argument('shipped_date', type=str, required=True, help='Shipped date is required')
order_post_args.add_argument('order_line_number', type=int, required=True, help='Shipped date is required')


class Order(Resource):
    def post(self):
        args = order_post_args.parse_args()
        res = add_order(args)
        return res
