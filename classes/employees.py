import os
import sys

sys.path.append(os.getcwd())
from flask import Flask, jsonify
from flask_restful import Api, Resource, reqparse, abort
from queries.emp_queries import get_all, add_employee, update_employee, delete_employee

employee_put_args = reqparse.RequestParser()
employee_put_args.add_argument("employeeNumber", type=int)
employee_put_args.add_argument("reportsTo", type=int)
employee_put_args.add_argument("lastName", type=str)
employee_put_args.add_argument("firstName", type=str)
employee_put_args.add_argument("extension", type=str)
employee_put_args.add_argument("email", type=str)
employee_put_args.add_argument("officeCode", type=str)
employee_put_args.add_argument("jobTitle", type=str)

employee_post_args = reqparse.RequestParser()
employee_post_args.add_argument("employeeNumber", type=int,
                                help="Should be employee number of the person :)", required=True)
employee_post_args.add_argument("lastName", type=str, help="Should be last name of the person :)", required=True)
employee_post_args.add_argument("firstName", type=str, help="Should be first name of the person :)", required=True)
employee_post_args.add_argument("extension", type=str, help="Should be extension :)", required=True)
employee_post_args.add_argument("email", type=str, help="Should be email of the person :)", required=True)
employee_post_args.add_argument("officeCode", type=str, help="Should be office-code of the person :)", required=True)
employee_post_args.add_argument("jobTitle", type=str, help="Should be office-code of the person :)", required=True)
employee_post_args.add_argument("reportsTo", type=int, help="report id is missing :)", required=True)


def parm_is_here(param):
    return True if param else False


class Employee(Resource):
    def get(self):
        employees = get_all()
        return jsonify(employees)

    def post(self):
        args = employee_post_args.parse_args()
        answer = add_employee(args)
        return ('Employee added', 201) if answer is True else (answer, 404)
        # Add this return in add_employee function

    def put(self, person_id=None):
        if parm_is_here(person_id):
            args = employee_put_args.parse_args()
            answer = update_employee(args, person_id)
            return answer
        abort(404, message="Missing param")

    def delete(self, person_id=None):
        if parm_is_here(person_id):
            answer = delete_employee(person_id)
            return answer
        abort(404, message="Missing param")
