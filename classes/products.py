import os
import sys

sys.path.append(os.getcwd())
from flask import Flask, jsonify
from flask_restful import Api, Resource, reqparse, abort
from queries.product_queries import add_product, get_product, update_product, delete_product, get_all

product_post_args = reqparse.RequestParser()
product_post_args.add_argument("productCode", type=str,
                               help="Failed with product code :)", required=True)
product_post_args.add_argument("productName", type=str, help="Failed with product name :)", required=True)
product_post_args.add_argument("productLine", type=str, help="Failed with  product line :)", required=True)
product_post_args.add_argument("productScale", type=str, help="Failed with product scale :)", required=True)
product_post_args.add_argument("productVendor", type=str, help="Failed with product vendor :)", required=True)
product_post_args.add_argument("productDescription", type=str, help="Failed with product description :)", required=True)
product_post_args.add_argument("quantityInStock", type=int, help="Failed with quantity of product in stock :)",
                               required=True)
product_post_args.add_argument("buyPrice", type=float, help="missing price :)", required=True)
product_post_args.add_argument("MSRP", type=float, help="missing MSRP :)", required=True)

product_spec_post_args = reqparse.RequestParser()
product_spec_post_args.add_argument("productCode", type=str, help="Failed with product code :)", required=True)

# za update product-a
product_put_args = reqparse.RequestParser()
product_put_args.add_argument("productCode", type=str)
product_put_args.add_argument("productName", type=str)
product_put_args.add_argument("productLine", type=str)
product_put_args.add_argument("productScale", type=str)
product_put_args.add_argument("productVendor", type=str)
product_put_args.add_argument("productDescription", type=str)
product_put_args.add_argument("quantityInStock", type=int)
product_put_args.add_argument("buyPrice", type=float)
product_put_args.add_argument("MSRP", type=float)


def parm_is_here(param):
    return True if param else False


class Product(Resource):
    def get(self):
        products = get_all()
        return jsonify(products)

    def post(self, product_code=None):
        # If you are working with true false logic, dont use strings
        if product_code == 'true':
            args = product_spec_post_args.parse_args()
            print(args)
            res = get_product(args.productCode)
            return jsonify(res[0]) if res[1] == 200 else (res[0], 404)

        args = product_post_args.parse_args()
        res = add_product(args)
        return ('Product added', 201) if res is True else (res, 404)

    def put(self, product_code=None):
        if parm_is_here(product_code):
            args = product_put_args.parse_args()
            answer = update_product(args, product_code)
            return answer
        abort(404, message="Missing param")

    def delete(self, product_code=None):
        if parm_is_here(product_code):
            answer = delete_product(product_code)
            return answer
        abort(404, message="Missing param")
