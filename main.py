from flask import Flask
from flask_restful import Api

from classes.customers import Customer_payment
from classes.emp_customer import Employees_customers
from classes.employees import Employee
from classes.orders import Order
from classes.products import Product

app = Flask(__name__)
api = Api(app)


api.add_resource(Customer_payment, '/customer/payment', '/customer/payment/<int:order_number>')
api.add_resource(Employees_customers, '/employees/customers')
api.add_resource(Employee, '/employees', '/employees/<string:person_id>')
api.add_resource(Product, '/products', '/products/<string:product_code>')
api.add_resource(Order, '/customer/order')
if __name__ == "__main__":
    app.run(debug=True)
