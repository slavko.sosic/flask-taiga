aniso8601==8.0.0
click==7.1.2
Flask==1.1.2
Flask-RESTful==0.3.8
Flask-SQLAlchemy==2.4.3
greenlet==3.0.3
itsdangerous==1.1.0
Jinja2==2.11.2
MarkupSafe==1.1.1
mysql-connector-python==8.3.0
pytz==2020.1
simplejson==3.19.2
six==1.15.0
SQLAlchemy==1.3.18
typing_extensions==4.9.0
Werkzeug==1.0.1
