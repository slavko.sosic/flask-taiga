import mysql.connector
from database.config import USER, PASSWORD
config = {
    'user':USER,
    'password': PASSWORD,
    'host': 'localhost',
    'database': 'classicmodels'
}

db = mysql.connector.connect(**config)
cursor = db.cursor()


